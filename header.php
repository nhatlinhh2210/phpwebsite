<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="author" content="Nhi Hae - Nhật Linh - Tố Uyên">
    
    <title>Tên gì cũng được</title>
    <link href="//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"/>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    
    <link rel="stylesheet" href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">

    <link href="<?php echo substr(dirname(__FILE__), strlen($_SERVER['DOCUMENT_ROOT']))?>/css/site.css" rel="stylesheet" />
</head>
<body>
    <div id="wrapper">
        <div class="container main-header-cate-search">
            <div class="row">
            <h2 class="text-info font-weight-bold"><img src="/LAB3/uploads/Adobe_Post_20210409_0816530.7655190371494675.png" width="120" height="50"></img>Ở ĐÂY CÓ BÁN SÁCH</h2>
            <!-- <center><h2 class="text-info font-weight-bold">Ở ĐÂY CÓ BÁN SÁCH</h2></center> </h2> -->
        </div>
        </div>
        <nav class="navbar bg-info navbar-primary"  role="navigation">
            <div class="container " >
                <div class="navbar-default"  >
                    <h4><a class="navbar-brand navbar-tex text-white" data-toggle="tab" role="tab" href="/LAB3/Views/list_product.php"> Danh sách sản phẩm</a> 
                    <a class="navbar-brand navbar-text text-white" data-toggle="tab" role="tab" href="/LAB3/Views/add_product.php"> Thêm sản phẩm</a>
                    <a class="navbar-brand navbar-text text-white" data-toggle="tab" role="tab" href="/LAB3/Views/delete_product.php"> Xóa sản phẩm</a>
                    </h4>
                    
                </div>
                
                <div class="navbar-header">
                    <?php
                    session_start();
                    if(isset($_SESSION['user'])!=""){
                        echo "<h4> Xin chào ".$_SESSION['user']."<a class='text-white' href='/LAB3/Views/logout.php'> Logout</a></h4>";
                    } else{
                        echo "<h4> Bạn chưa đăng nhập <a class='text-white' href='/LAB3/Views/login.php'>Login</a> - <a class='text-white' href='/LAB3/Views/register.php'>Register</a></h4>";
                    }
                    ?>
                </div>
                <div class="container "> 
                    <form class="navbar-form" role="search">
                        <div class="input-group"> 
                            <input type="hidden" name="search_param" value="all" id="search_param"> <input type="text" class="form-control" name="keyword" placeholder="Tìm kiếm sản phẩm mong muốn …"> <span class="input-group-btn">
                                <button class="btn btn-default text-white" type="button"><span class="glyphicon glyphicon-search"></span> Tìm kiếm</button>
                        </div> 
                    </form>
                </div>
            </div>
            </div>
            </div>
        </nav>

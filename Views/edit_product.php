<?php 
    require_once("../Models/product.class.php");
    require_once("../Models/category.class.php");
    require_once('../header.php');



      $id = $_GET['edit_id'];

      $product = Product::get_product($id);
      $prod = reset($product);
      $link_anh="/LAB3/uploads/".$prod["Picture"];

      // echo $prod['Description'];
      if(isset($_POST["btn_edit"])){
        $productName = $_POST['txtName'];
        $cateID = $_POST['txtCateID'];
        $price = $_POST['txtprice'];
        $description = $_POST['txtdesc'];
        $quantity = $_POST['txtquantity'];
        $picture = $prod["Picture"];
        // echo $description;
        $newProduct = new Product($productName,$cateID,$price,$quantity,$description,$picture);

        $result = $newProduct->edit_product($id);

        if(!$result){
          header("Location: edit_product.php?failure");
        } else{
            header("Location: edit_product.php?edited");
        }

        
    }

    if(isset($_GET["edited"])){
      echo "<h2>Chỉnh sửa sản phẩm thành công</h2>";
    }   


    // if (isset($_POST["btn_edit"])) {
    //   $sqlUpdate = "UPDATE `product` SET ProductName='$productName',CateID='$cateID',Price='$price',Quantity='$quantity',Description='$desc',Picture='$picture' WHERE ProductID=$id";
    //   $db = new Db();
    //   $db->query_execute($sqlUpdate);
    //   header('Location: list_product.php');
    // }
    
?>

<style>
  .image{
    width: 600px;
    height: 700px;
  }
  .form-control:focus {
  background-color: #f2f2f2;
  box-shadow: none;
  color: #000;
  border-color: #4f63e7;
}

</style>

<form method="post" >
  <div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
    <div class="card card0 border-0">
        <div class="row d-flex">
            <div class="col-lg-6">
                <div class="card1 pb-5">
                    <div class="row px-3 justify-content-center mt-4 mb-5 border-line">
                      <img 
                      src="<?php echo $link_anh ?>"
                      class="image" >
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card2 card border-0 px-4 py-5">
                <h5  style="margin: auto; text-align:center; font-size:30px; font-weight:bold">Chỉnh sửa sản phẩm</h5>
                <div class="card px-5 py-5">
                  <div class="col-md-12">
                    <label for="inputEmail4" class="form-label" style="margin: auto; text-align:center; font-size:16px; font-weight:bold">Tên sản phẩm</label>
                    <input class="form-control" type="text" name="txtName" value="<?php echo isset($_POST["txtName"]) ? $_POST["txtName"] : "" ?>" placeholder="<?php echo $prod['ProductName']; ?>" required/>
                  </div>
                  <div class="col-md-12">
                    <label for="inputAddress" class="form-label" style="margin: auto; text-align:center; font-size:16px; font-weight:bold">Mô tả sản phẩm</label>
                    <textarea class="form-control" name="txtdesc" cols="30" rows="20" value="<?php echo isset($_POST["txtdesc"]) ? $_POST["txtdesc"] : "" ?>" placeholder="<?php echo $prod['Description'] ?>" required></textarea>
                  </div>
                  <div class="col-6">
                    <label for="inputAddress2" class="form-label" style="margin: auto; text-align:center; font-size:16px; font-weight:bold">Số lượng sản phẩm</label>
                    <input class="form-control" type="text" name="txtquantity" value="<?php echo isset($_POST["txtquantity"]) ? $_POST["txtquantity"] : ""; ?>" placeholder="<?php echo $prod['Quantity'] ?>" required/>
                  </div>

                  <div class="col-md-6">
                    <label for="inputCity" class="form-label" style="margin: auto; text-align:center; font-size:16px; font-weight:bold">Giá bán</label>
                    <div class="input-group col-md-6">
                      <input type="text" class="form-control" name="txtprice" value="<?php echo isset($_POST["txtprice"]) ? $_POST["txtprice"] : "";?>" placeholder="<?php  echo $prod['Price'] ?>" required>
                      <span class="input-group-text">VND</span>
                    </div>
                  </div>

                  <div class="col-md-6">
                    <label for="inputState" class="form-label" style="margin: auto; text-align:center; font-size:16px; font-weight:bold">Loại sản phẩm</label>
                    <select id="inputState" class="form-select" name="txtCateID">
                        <option value="" >--Chọn loại--</option>
                            <?php
                              $cates = Category::list_category();
                                foreach($cates as $item){ ?>

                                  <option <?php echo $item["CateID"] == $prod["CateID"] ?"selected":""  ?>  value="<?php echo $item["CateID"]?>"><?php echo $item["CategoryName"]?></option>;
                          
                               <?php }?>
                        </select>
                    </select>
                  </div>
                  <div class="col-md-6">
                    <label for="inputZip" class="form-label" style="margin: auto; font-size:16px; font-weight:bold">Chọn ảnh </label>
                    <label for="note" class="form-label" style="margin: auto; font-size:14px;"> (Nếu không thay đổi thì để trống)</label>
                    <input type="file" class="form-control" name="txtpic" accept=".PNG,.GIF,.JPG" value="<?php echo $prod['Picture']; ?>"/>
                  </div>
                  <div class="col-12">
                    <button type="submit" name="btn_edit" class="btn btn-primary">Chỉnh sửa</button>
                  </div>
                  </div>
            </div>
        </div>
    </div>
  </div>
  </form>

<?php require_once('../footer.php'); ?>
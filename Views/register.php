<?php
    include_once('../header.php');
?>
<?php
if(isset($_SESSION['user'])!=""){
    header("Location: ../index.php");
}
require_once("../Models/user.class.php");
if(isset($_POST['btn-signup'])){
    $u_name = $_POST['txtname'];
    $u_email = $_POST['txtemail'];
    $u_pass = $_POST['txtpass'];
    $account = new User($u_name, $u_email, $u_pass);
    $result = $account->save();
    if(!$result){
        ?>
        <script>alert('Có lỗi xảy ra, vui lòng kiểm tra dữ liệu')</script>
        <?php
    } else{
        $_SESSION['user'] = $u_name;
        header("Location: ../index.php");
    }
}
?>



<!-- <form method="post" style="width:30%">
    <div class="form-group row">
        <label for="txtname" class="col-sm-2 form-control-label">UserName</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" name="txtname" placeholder="User name">
        </div>
    </div>
    <div class="form-group row">
        <label for="txtemail" class="col-sm-2 form-control-label">Email</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" name="txtemail" placeholder="Email">
        </div>
    </div>
    <div class="form-group row">
        <label for="txtpass" class="col-sm-2 form-control-label">Password</label>
        <div class="col-sm-10">
            <input type="password" class="form-control" name="txtpass" placeholder="Password">
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-offset-2 col-sm-10">
            <input type="submit" name="btn-signup" value="Sign Up">
        </div>
    </div>
</form> -->
<form method="post" class="needs-validation" >
<div class="container mt-5 mb-5">
    <div class="row d-flex align-items-center justify-content-center">
        <div class="col-md-6">
            <div class="card px-5 py-5">
                <h5 class="mt-3" style="margin: auto; text-align:center; font-size:30px; font-weight:bold">Đăng ký <br>Siêu ưu đãi mỗi ngày</h5>
                <div class="form-input"> 
                    <i class="fa fa-envelope"></i> 
                    <input type="text" name="txtemail" class="form-control" placeholder="Email" required>
                 </div>
                <div class="form-input"> 
                    <i class="fa fa-user"></i> 
                    <input type="text" name="txtname" class="form-control" placeholder="Tài khoản" required> 
                </div>
                <div class="form-input"> 
                    <i class="fa fa-lock"></i> 
                    <input type="text" name="txtpass" class="form-control" placeholder="Mật khẩu" required> 
                </div>
                <div class="form-check"> 
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" unchecked required>
                    <label class="form-check-label" for="flexCheckChecked"> Tôi đồng ý với các điều khoản </label>
                </div> 
                  <button class="btn btn-primary mt-4 signup" name="btn-signup" type="submit">Đăng ký tài khoản</button>
                <div class="text-center mt-3"> 
                <span>Hoặc đăng nhập bằng các tài khoản sau</span> 
                </div>
                <div class="d-flex justify-content-center mt-4"> 
                    <span class="social"><i class="fa fa-google"></i></span>
                    <span class="social"><i class="fa fa-facebook"></i></span> 
                    <span class="social"><i class="fa fa-twitter"></i></span> 
                    <span class="social"><i class="fa fa-linkedin"></i></span> 
                </div>
                <div class="text-center mt-4">
                    <span>Đã có tài khoản?</span>
                    <a href="login.php" class="text-decoration-none">Đăng nhập ngay</a>
                </div>
            </div>
        </div>
    </div>
</div>
</form>

<?php include_once("../footer.php")?>
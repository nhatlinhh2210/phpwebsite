<?php

require_once("../Models/product.class.php");
require_once("../Models/category.class.php");
include_once("../header.php");


if(isset($_POST["btnsubmit"])){

    $productName = $_POST["txtName"];
    $price = $_POST["txtprice"];
    $cateID = $_POST["txtCateID"];
    $quantity = $_POST["txtquantity"];
    $description = $_POST["txtdesc"];
    $picture = $_FILES["txtpic"];

    $newProduct = new Product($productName, $cateID, $price, $quantity, $description, $picture);

    $result = $newProduct->save();

    if(!$result){
        header("Location: add_product.php?failure");
    } else{
        header("Location: add_product.php?inserted");
    }

    // print_r($_POST);
    // var_dump($result);
}

if(isset($_GET["inserted"])){
  echo "<h2>Thêm sản phẩm thành công</h2>";
}   

?>

<form class="row g-3 mt-5 needs-validation" style="max-width: 500px; margin: auto;" method="post" enctype="multipart/form-data" >
  <p style="font-size: 24px; font-style:initial; font-weight:bold">Thêm sản phẩm</p>
  <div class="card px-5 py-5">
  <div class="col-md-12">
    <label for="inputEmail4" class="form-label">Tên sản phẩm</label>
    <input class="form-control" type="text" name="txtName" value="<?php echo isset($_POST["txtName"]) ? $_POST["txtName"] : ""; ?>" required/>
  </div>
  <div class="col-12">
    <label for="inputAddress" class="form-label">Mô tả sản phẩm</label>
    <textarea class="form-control" name="txtdesc" cols="21" rows="10" value="<?php echo isset($_POST["txtdesc"]) ? $_POST["txtdesc"] : "";?>" required></textarea>
  </div>
  <div class="col-6">
    <label for="inputAddress2" class="form-label">Số lượng sản phẩm</label>
    <input class="form-control" type="text" name="txtquantity" value="<?php echo isset($_POST["txtquantity"]) ? $_POST["txtquantity"] : ""; ?>" required/>
  </div>

  <div class="col-6">
    <label for="inputCity" class="form-label">Giá bán</label>
    <div class="input-group col-md-6">
      <input type="text" class="form-control" name="txtprice" aria-label="Amount (to the nearest dollar)" value="<?php echo isset($_POST["txtprice"]) ? $_POST["txtprice"] : ""; ?>" required>
      <span class="input-group-text">VND</span>
    </div>
  </div>

  <div class="col-md-6">
    <label for="inputState" class="form-label">Loại sản phẩm</label>
    <select id="inputState" class="form-select" name="txtCateID">
        <option value="" selected required>--Chọn loại--</option>
            <?php
              $cates = Category::list_category();
                foreach($cates as $item){
                  echo "<option value=".$item["CateID"].">".$item["CategoryName"]."</option>";
                }
            ?>
        </select>
    </select>
  </div>
  <div class="col-md-6">
    <label for="inputZip" class="form-label">Chọn ảnh</label>
    <input type="file" class="form-control" name="txtpic" accept=".PNG,.GIF,.JPG" required/>
  </div>
  <div class="col-12">
    <button type="submit" name="btnsubmit" class="btn btn-primary">Thêm sản phẩm</button>
  </div>
  </div>
</form>


<?php include_once("../footer.php");?>
<?php

require_once("../config/db.class.php");
require_once("../header.php");
session_start();
?>

<?php
	//Gọi file connection.php ở bài trước
	require_once("../header.php");
	require_once("../Models/user.class.php");
	// Kiểm tra nếu người dùng đã ân nút đăng nhập thì mới xử lý
	if (isset($_POST["btn_submit"])) {
		// lấy thông tin người dùng
		$username = $_POST["username"];
		$password = $_POST["password"];
		//làm sạch thông tin, xóa bỏ các tag html, ký tự đặc biệt 
		//mà người dùng cố tình thêm vào để tấn công theo phương thức sql injection
		$username = strip_tags($username);
		$username = addslashes($username);
		$password = strip_tags($password);
		$password = addslashes($password);
		if ($username == "" || $password =="") {
			echo "username hoặc password bạn không được để trống!";
		}else{
			$account = new User($u_name, $u_email, $u_pass);
			$result = $account->checkLogin($username,$password);
			//$sql = "select * from users where username = '$username' and password = '$password' ";
			//$result = $db->query_execute($sql);
			//$query = mysqli_query($db,$sql);
			//$num_rows = mysqli_num_rows($result);
			if ($result==0) {
				echo "tên đăng nhập hoặc mật khẩu không đúng !";
			}else{
				//tiến hành lưu tên đăng nhập vào session để tiện xử lý sau này
				$_SESSION['user'] = $username;
                // Thực thi hành động sau khi lưu thông tin vào session
                // ở đây mình tiến hành chuyển hướng trang web tới một trang gọi là index.php
                header('Location: ../index.php');
                
			}
		}
	}
?>
	<!-- <form method="POST" action="login.php">
	<fieldset>
	    <legend>Đăng nhập</legend>
	    	<table>
	    		<tr>
	    			<td>Username</td>
	    			<td><input type="text" name="username" size="30"></td>
	    		</tr>
	    		<tr>
	    			<td>Password</td>
	    			<td><input type="password" name="password" size="30"></td>
	    		</tr>
	    		<tr>
	    			<td colspan="2" align="center"> <input type="submit" name="btn_submit" value="Đăng nhập"></td>
	    		</tr>
	    	</table>
  </fieldset>
  </form> -->

  <!-- <form action="login.php" method="post">
    <div class="form-group">
      <label for="email">Username:</label>
      <input  class="form-control" id="email" name="username" placeholder="Enter email" size="30" >
    </div>
    <div class="form-group">
      <label for="pwd">Password:</label>
      <input type="password" class="form-control" id="pwd" name="password" placeholder="Enter password" size="30">
    </div>
    <div class="form-group form-check">
      <label class="form-check-label">
        <input class="form-check-input" type="checkbox" name="remember"> Remember me
      </label>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
  </form> -->
<!-- 
  <form action="login.php" method="post">
  <div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
    <div class="card card0 border-0">
        <div class="row d-flex">
            <div class="col-lg-6">
                <div class="card1 pb-5">
                    <div class="row"> <img src="https://i.imgur.com/CXQmsmF.png" class="logo"> </div>
                    <div class="row px-3 justify-content-center mt-4 mb-5 border-line"> <img src="https://i.imgur.com/uNGdWHi.png" class="image"> </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card2 card border-0 px-4 py-5">
				<div class="row mb-4 px-3">
                    <div class="row px-3"> <label class="mb-1">
                            <h6 class="mb-0 text-sm">Username</h6>
                        </label> <input class="mb-4" type="text" name="username" placeholder="Enter a valid username"> </div>
                    <div class="row px-3"> <label class="mb-1">
                            <h6 class="mb-0 text-sm">Password</h6>
                        </label> <input type="password" name="password" placeholder="Enter password"> </div>
                    <div class="row px-3 mb-4">
                        <div class="custom-control custom-checkbox custom-control-inline"> <input id="chk1" type="checkbox" name="chk" class="custom-control-input"> <label for="chk1" class="custom-control-label text-sm">Remember me</label> </div> <a href="#" class="ml-auto mb-0 text-sm">Forgot Password?</a>
                    </div>
                    <div class="row mb-3 px-3"> <button type="submit" name="btn_submit" class="btn btn-primary" color>Login</button> </div>
                    <div class="row mb-4 px-3"> <small class="font-weight-bold">Don't have an account? <a class="text-danger " href="register.php">Register</a></small> </div>
                </div>
            </div>
        </div>
    </div>
</div>
  </div>
  </form> --> 

<form method="post" class="needs-validation" >
<div class="container mt-5 mb-5">
    <div class="row d-flex align-items-center justify-content-center">
        <div class="col-md-6">
            <div class="card px-5 py-5">
                <h5 class="mt-3" style="margin: auto; text-align:center; font-size:30px; font-weight:bold">Đăng Nhập</h5>
                <div class="form-input"> 
                    <i class="fa fa-user"></i> 
                    <input type="text" name="username" class="form-control" placeholder="Tài khoản" required> 
                </div>
                <div class="form-input"> 
                    <i class="fa fa-lock"></i> 
                    <input type="password" name="password" class="form-control" placeholder="Mật khẩu" required> 
                </div>
                <div class="form-check"> 
                    <input class="form-check-input" type="checkbox" value="" id="flexCheckChecked" unchecked style="padding: 10px;">
                    <label class="form-check-label" for="flexCheckChecked"> Lưu tài khoản </label>
                </div> 
                  <button class="btn btn-primary mt-4 signup" name="btn_submit" type="submit">Đăng nhập</button>
                <div class="text-center mt-3"> 
                    <span>Hoặc đăng nhập bằng các tài khoản sau</span> 
                </div>
                <div class="d-flex justify-content-center mt-4"> 
                    <span class="social"><i class="fa fa-google"></i></span>
                    <span class="social"><i class="fa fa-facebook"></i></span> 
                    <span class="social"><i class="fa fa-twitter"></i></span> 
                    <span class="social"><i class="fa fa-linkedin"></i></span> 
                </div>
                <div class="text-center mt-4">
                    <span>Chưa có tài khoản?</span>
                    <a href="register.php" class="text-decoration-none">Đăng ký</a>
                </div>
            </div>
        </div>
    </div>
</div>
</form>



<?php require_once("../footer.php"); ?>
<?php include_once('../header.php')?>
<?php
    require_once("../Models/product.class.php");
    require_once("../Models/category.class.php");
    $cates = Category::list_category();
   if(!isset($_SESSION))
   {
       session_start();
   }
    error_reporting(E_ALL);
    ini_set('display_errors', '1');

    if(isset($_GET["id"])){
        $pro_id = $_GET["id"];
        $was_found = false;
        $i = 0;
        
        if(!isset($_SESSION["cart_items"]) || count($_SESSION["cart_items"])<1){
            $_SESSION["cart_items"] = array(0 => array("pro_id" => $pro_id, "quantity" => 1));
        } else{
            foreach($_SESSION["cart_items"] as $item){
                $i++;
                //reset($item);
               foreach($item as $key => $value){
                    if($key=="pro_id" && $value==$pro_id){
                        array_splice($_SESSION["cart_items"], $i-1, 1, array(array("pro_id" => $pro_id, "quantity" => $item["quantity"]+1)));
                        $was_found = true;
                    }
                }
            }
            if($was_found==false){
                array_push($_SESSION["cart_items"], array("pro_id" => $pro_id, "quantity" =>1));
            }
        }
        header("location: /LAB3/Views/shopping_cart.php");
    }
?>
<div class="container text-center">


<nav class="navbar navbar-expand-lg navbar-dark bg-dark text-white" style="margin: auto;">
    <div class="container">
        <!-- <div class="col-sm-3"> -->
        <h3> Danh mục</h3>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#my-nav-bar" aria-controls="my-nav-bar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <ul class="collapse navbar-collapse" id="my-nav-bar" ><ul class="navbar-nav">
            <?php
                foreach($cates as $item){
                    echo "<li class='nav-item'><a class='navbar-brand navbar-text' style='width:auto'
                    href=/LAB3/Views/list_product.php?cateid=".$item["CateID"].">".$item["CategoryName"]."</a> </li>";
                }
            ?>
        </ul>
    </div>
</div>
</nav>

<div class="container px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto" >
    <div class="card card0 border-0" style="align-items: center;">
    <div class="col-sm-9">
        <h3>Thông tin giỏ hàng</h3><br>
        <table class="table table-condensed">
            <thead>
                <tr>
                    <th>Tên sản phẩm</th>
                    <th>Hình ảnh</th>
                    <th>Số lượng</th>
                    <th>Đơn giá</th>
                    <th>Thành tiền</th>
                    <th>Bỏ</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $total_money = 0;
                    if(isset($_SESSION["cart_items"]) && count($_SESSION["cart_items"])>0){
                        foreach($_SESSION["cart_items"] as $item){
                            
                            $id = $item["pro_id"];
                            $product = Product::get_product($id);
                            $prod = reset($product);
                            $total_money += $item["quantity"]*$prod["Price"];
                            $link_anh="/LAB3/uploads/".$prod["Picture"];
                            echo "<tr>
                                <td>".$prod["ProductName"]."</td>
                                <td><img style='width:90px; height:80px' src=".$link_anh."
                                /></td>
                                <td>".$item["quantity"]."</td>
                                <td>".$prod["Price"]."</td>
                                <td>".$prod["Price"]."</td>
                                <td><a href='/Views/shopping_cart.php?delete_product=$id' name='delete' class='btn btn-default'>
                                <i> Delete </i>  
                            </a></td>
                                </tr>";

                        }
                        
                        if(isset($_GET["cart_items"] ) and $_GET["cart_items"]== "delete")
                        {
                            for($id=0; $id<count($_SESSION['cart_items']);$id++)
                            {
                                if($_SESSION['cart_items'][$id]["idProduct"]==$_GET['ProductID'])
                                {
                                    $_SESSION['cart_items'][$id]["ProductID"] = NULL;
                                }
                            }
                        }
                        ?>

                        <tr><td colspan=5><p class='text-right text-danger'> Tổng tiền:<?php echo $total_money ?></p></td></tr>
                        <tr><td colspan=3><p class='text-right'><button type='button' class='btn btn-primary' onclick="location.href='/LAB3/Views/list_product.php'">Tiếp tục mua hàng</button></p></td><td colspan=2><p class='text-right'><button type='button' class='btn btn-success'>Thanh toán</button></p></td></tr>
                        <?php 
                    } else{
                        echo "Không có sản phẩm nào trong giỏ hàng!";
                    }
                ?>
            </tbody>
        </table>
    </div>
    </div>
</div>

<?php include_once("../footer.php");?>
<?php
    require_once("../Models/product.class.php");
    require_once("../Models/category.class.php");
?>

<?php
include_once("../header.php");
if(!isset($_GET["id"])){
    header('Location: not_found.php');
} else{
    $id = $_GET["id"];
    $prod = Product::get_product($id);
    $prods_relate = Product::list_product_relate($prod[0]["CateID"],$id);
   
}
    $cates = Category::list_category();
    $id = $_GET["id"];
    $product = Product::get_product($id);
    $prods = reset($product);
    $link_anh="/LAB3/uploads/".$prods["Picture"]; 
?>

<style>
    .image{
        width: 600px;
        height: 600px;
    }

    .card-title{
        display: inline-flexbox;
        width: 320px;
        white-space: nowrap;
        overflow: hidden !important;
        text-overflow: ellipsis;
        height: 50px;
        margin-left: 20px;
    }

</style>

<div class="container text-center">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark text-white">
    <div class="container">
        <!-- <div class="col-sm-3"> -->
        <h3> Danh mục</h3>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#my-nav-bar" aria-controls="my-nav-bar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <ul class="collapse navbar-collapse" id="my-nav-bar" ><ul class="navbar-nav">
            <?php
                foreach($cates as $item){
                    echo "<li class='nav-item'><a class='navbar-brand navbar-text' style='width:auto'
                    href=/LAB3/Views/list_product.php?cateid=".$item["CateID"].">".$item["CategoryName"]."</a> </li>";
                }
            ?>
        </ul>
    </div>
</div>
</nav>

<div class="container px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
    <div class="card card0 border-0">
        <div class="row d-flex">
            <div class="col-lg-6">
                <div class="card1 pb-5">
                    <div class="row px-3 justify-content-center mt-4 mb-5 border-line">
                      <img 
                      src="<?php echo $link_anh ?>"
                      class="image" >
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div style="padding-left: 10px;">
                    <h3 class="text-info">
                        <?php echo $prod[0]["ProductName"];?>
                    </h3>
                    <p >

                    Mô tả: <?php echo $prod[0]["Description"]?>
                    </p>
                    <p style="font-weight: bold;">

                    Giá: <?php echo $prod[0]["Price"]?> VND
                    </p>
                    <p>
                        <button type="button" class="btn btn-danger">Mua hàng</button>
                    </p>
                </div>
            </div>
        </div>
       

    </div>
    <h3 class="panel-heading " style="padding:10px">Sản phẩm liên quan</h3>

    <div class="container">
        <div class="row">
                <?php
                    foreach($prods_relate as $item){
                        ?>
                        <div class="col">
                            <div class="card text-center"  >
                                <a href="/LAB3/Views/product_detail.php?id=<?php echo $item["ProductID"];?>">
                                    <img src="<?php echo "/LAB3/uploads/".$item["Picture"];?>" class="img-responsive" style="width:300px; height: 300px; align-content:center"  alt="Image"/>
                                </a>
                                <div class="card-body">
                                    <p class="card-title" title="<?php echo $item["ProductName"];?>" style="font-size: 18px;"><?php echo $item["ProductName"];?></p>
                                    <p class="card-text" style="font-weight: bold;"><?php echo $item["Price"];?> VND</p>

                                    <button type="button" class="btn btn-danger" onclick="location.href='/LAB3/Views/shopping_cart.php?id=<?php echo $item['ProductID'] ?>'">Mua hàng</button>
                                    
                                </div>
                            </div>
                        </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php include_once("../footer.php")?>
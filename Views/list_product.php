<?php 
    require_once("../Models/product.class.php");
    require_once("../Models/category.class.php");
    include_once("../header.php");
?>
<?php
    if(!isset($_GET["cateid"])){
        $prods = Product::list_product();
    } else{
        $cateid = $_GET["cateid"];
        $prods = Product::list_product_by_cateid($cateid);
    }
    $cates = Category::list_category();
?>
<style >

.card-title {
    display: inline-flexbox;
    width : 100%;
    white-space: nowrap;
    overflow: hidden !important;
    text-overflow: ellipsis;
    height: 50px;
    size: 20px;
}

.card-img-top{
    padding: 10px;
}

</style>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark text-white">
    <div class="container">
        <!-- <div class="col-sm-3"> -->
        <h3> Danh mục</h3>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#my-nav-bar" aria-controls="my-nav-bar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <ul class="collapse navbar-collapse" id="my-nav-bar" ><ul class="navbar-nav">
            <?php
                foreach($cates as $item){
                    echo "<li class='nav-item'><a class='navbar-brand navbar-text' style='width:auto'
                    href=/LAB3/Views/list_product.php?cateid=".$item["CateID"].">".$item["CategoryName"]."</a> </li>";
                }
            ?>
        </ul>
    </div>
</div>
</nav>
<div class="container text-center">
    <!-- <div class="col-sm-9"> -->
    <h3> </h3>
    <h3>Sản phẩm cửa hàng</h3><br>
    <div class="row">
        <?php
            foreach($prods as $item){
        ?>
        
            <!-- <div class="col-sm-4">
                <div class="row">
                    <a href="/LAB3/Views/product_detail.php?id= <?php echo $item["ProductID"];?>">
                        <img src="<?php echo "/LAB3/".$item["Picture"];?>" class="img-responsive" style="width:200px; height=250px" alt="Image">
                    </a>
                </div>
                <div class="row">
                    <p class="text-danger"><?php echo $item["ProductName"];?></p>
                </div>
                <div class="row">
                <p class="text-info"><?php echo $item["Price"];?></p>
                </div>
                <div class="row">
                    <p> 
                        <button type="button" class="btn btn-primary" onclick="location.href='/LAB3/Views/shopping_cart.php?id= <?php echo $item["ProductID"] ?>'">Mua hàng</button>
                    </p>
                </div>
                <div class="row">
                    <p> 
                        <button type="button" class="btn btn-primary" onclick="location.href='/LAB3/Views/delete_product.php?delete_id= <?php echo $item['ProductID'] ?>'">Xóa</button>
                    </p>
                </div>
            </div> -->

            <div class="card" style="width: 18rem; margin: auto; margin-top: 10px;">
            <img class="card-img-top" src="<?php 
                $link_anh="/LAB3/uploads/".$item["Picture"];
                echo $link_anh ?>" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title" title="<?php echo $item["ProductName"];?>"><?php echo $item["ProductName"];?></h5>
                <p class="card-text" style="font-size: 20px; font-weight:bold"><?php echo $item["Price"];?> VND</p>
                <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->

            </div>

            <div class="card-body">
                <button type="button" class="btn btn-primary" onclick="location.href='/LAB3/Views/shopping_cart.php?id=<?php echo $item['ProductID'] ?>'">Mua hàng</button>
                <button type="button" class="btn btn-primary" onclick="location.href='/LAB3/Views/product_detail.php?id=<?php echo $item['ProductID'] ?>'">Chi tiết</button>
            </div>
            

            <div class="card-body">
                <button type="button" class="btn btn-primary" onclick="location.href='/LAB3/Views/delete_product.php?delete_id=<?php echo $item['ProductID'] ?>'">Xóa</button>
                <button type="button" class="btn btn-primary" onclick="location.href='/LAB3/Views/edit_product.php?edit_id=<?php echo $item['ProductID'] ?>'">Chỉnh sửa</button>
            </div>
    
            </div>

            <?php } ?>
    </div>
</div>
</div>

<?php
    include_once("../footer.php");
?>
-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th4 09, 2021 lúc 04:26 PM
-- Phiên bản máy phục vụ: 10.4.18-MariaDB
-- Phiên bản PHP: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `ecommercee`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `CateID` int(11) NOT NULL,
  `CategoryName` varchar(150) NOT NULL,
  `Description` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`CateID`, `CategoryName`, `Description`) VALUES
(1, 'Văn học Nước ngoài', 'Tác phẩm văn học, tiểu thuyết, truyện dài, truyện ngắn của nước ngoài'),
(2, 'Sách kỹ năng ', 'Sản phẩm nói về những kỹ năng cần thiết'),
(3, 'Văn học Việt Nam', 'Tác phẩm văn học, Truyện Dài, Truyện Ngắn của Việt Nam'),
(7, 'Sách Thiếu Nhi', 'Sách, Truyện tranh, SGK dành cho thiếu nhi'),
(8, 'Tạp chí - Văn phòng phẩm ', 'Tạp chí - Văn phòng phẩm '),
(9, 'Sách Kinh tế', 'Sách Kinh tế'),
(10, 'Sách Chuyên ngành', 'Sách Chuyên ngành');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orderdetail`
--

CREATE TABLE `orderdetail` (
  `OrderID` int(11) NOT NULL,
  `ProductID` int(11) NOT NULL,
  `Quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orderproduct`
--

CREATE TABLE `orderproduct` (
  `OrderID` int(11) NOT NULL,
  `OrderDate` datetime NOT NULL,
  `ShipDate` datetime NOT NULL,
  `ShipName` varchar(150) NOT NULL,
  `ShipAddress` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `ProductID` int(11) NOT NULL,
  `ProductName` varchar(150) DEFAULT NULL,
  `CateID` int(11) DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `Quantity` int(11) DEFAULT NULL,
  `Description` varchar(500) DEFAULT NULL,
  `Picture` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`ProductID`, `ProductName`, `CateID`, `Price`, `Quantity`, `Description`, `Picture`) VALUES
(1, 'Ta Bắt Đầu Cuộc Đời Mới, Khi Nhận Ra Mình Chỉ Sống Một Lần', 1, 87000, 11, 'Cuốn sách đã bán được hơn hai triệu bản và được chuyển ngữ tại hơn ba mươi quốc gia. Phiên bản sách bỏ túi liên tiếp nằm trong top năm cuốn sách bán chạy nhất của Pocket, thành công chưa từng có kể từ Mật mã Da Vinci (2015).\r\n\r\nCamille, ba mươi tám tuổi một phần tư, có tất cả mọi thứ để sống hạnh phúc nhưng lại cảm thấy hạnh phúc đang trượt khỏi tay mình. Cô phải đối mặt với những vấn đề cuộc sống thường nhật mà bất cứ ai trong chúng ta cũng từng gặp phải: cuộc sống gia đình và công việc nhàm ch', 'uploads/202104060650370b66b99912f2daf81b09fdde03a9a722(1).jpg'),
(2, 'Biên niên cô đơn', 1, 64000, 100, '“Mất bao lâu để quên một người?!\r\n\r\nSau khi chia tay người cũ, mình mất hết một năm loay hoay trong mớ bòng bong cảm xúc trong lòng. Lúc đó không buồn, cũng chẳng quá đau khổ dằn vặt gì, vẫn cứ sống và làm việc bình thường, thậm chí làm việc còn nhiều hơn trước và thành công hơn trước.\r\n\r\nNhưng vết xước trong lòng chưa bao giờ lành lặn.\r\n\r\nMỗi khi đi ngang một con đường cũ, một quán ăn quen, một điều gì đó cứ nhói lên, rồi lại mất hút. Rõ ràng không thể gọi đó là cơn đau, chỉ là cái khẽ rùng mìn', 'uploads/202104060652134df230b5dbd95ef93e67d0b18df28d7a.jpg'),
(3, 'Bước Chậm Lại Giữa Thế Gian Vội Vã ', 1, 63000, 50, 'Chen vai thích cánh để có một chỗ bám trên xe buýt giờ đi làm, nhích từng xentimét bánh xe trên đường lúc tan sở, quay cuồng với thi cử và tiến độ công việc, lu bù vướng mắc trong những mối quan hệ cả thân lẫn sơ… bạn có luôn cảm thấy thế gian xung quanh mình đang xoay chuyển quá vội vàng?\r\n\r\nNếu có thể, hãy tạm dừng một bước.\r\n\r\nĐể tự hỏi, là do thế gian này vội vàng hay do chính tâm trí bạn đang quá bận rộn? Để cầm cuốn sách nhỏ dung dị mà lắng đọng này lên, chậm rãi lật giở từng trang, thong ', 'uploads/202104060654122f70de3ea7eec9c34f55e402254e27ed.jpg'),
(4, 'Cánh đồng bất tận', 3, 59900, 100, 'Cánh đồng bất tận bao gồm những truyện hay và mới nhất của nhà văn Nguyễn Ngọc Tư. Đây là tác phẩm đang gây xôn xao trong đời sống văn học, bởi ở đó người ta tìm thấy sự dữ dội, khốc liệt của đời sống thôn dã qua cái nhìn của một cô gái. Bi kịch về nỗi mất mát, sự cô đơn được đẩy lên đến tận cùng, khiến người đọc có lúc cảm thấy nhói tim...\r\n\r\nGiá sản phẩm trên Tiki đã bao gồm thuế theo luật hiện hành. Tuy nhiên tuỳ vào từng loại sản phẩm hoặc phương thức, địa chỉ giao hàng mà có thể phát sinh t', 'uploads/202104060655492410e2b33f6a496f02587d9ebcff2a2f.jpg'),
(5, 'Combo Phát Triển Toàn Diện - Bộ sách 5 Cuốn Kỹ Năng Sống Dành Cho Trẻ', 7, 250000, 100, 'Bộ Sách Kĩ Năng Sống Dành Cho Trẻ - Mình Không Để Mẹ Lo Lắng\r\nBộ Sách Kĩ Năng Sống Dành Cho Trẻ - Mình Có Rất Nhiều Bạn Tốt\r\nBộ Sách Kĩ Năng Sống Dành Cho Trẻ - Mình Nhất Định Sẽ Thành Công\r\nBộ Sách Kĩ Năng Sống Dành Cho Trẻ - Mình Có Làm Việc Không Phân Tâm\r\nBộ Sách Kĩ Năng Sống Dành Cho Trẻ - Mình Có Rất Nhiều Thói Quen Tốt', 'uploads/20210406065740015ffb45d27e3149c8e24a0ccd83177f.jpg'),
(6, 'Sách dạy con về tiền và tài chính dành cho trẻ từ 5-10 tuổi - Những câu hỏi vì sao về tiền và tài chính', 2, 75000, 100, 'Dạy con những khái niệm về tiền và tài chính là một trong những kỹ năng vô cùng quan trọng và cần thiết cho trẻ trong độ tuổi từ 5-10. Tuy nhiên, thực tế không phải phụ huynh nào cũng quan tâm và dành thời gian dạy con, trả lời thắc của con những khái niệm này. \r\n\r\nVì sao vậy? vì chúng ta nghĩ rằng trẻ trong độ tuổi từ 5-10 còn quá sớm để dạy con những khái niệm này, hay chúng ta nghĩ rằng trẻ có thể tự tìm hiểu mà không cần cha mẹ phải hướng dẫn?\r\n\r\nNếu phụ huynh nào đã nghĩ như thế thì giờ là ', 'uploads/20210406065954a8976727db2a059a623b324a7b895730.jpg'),
(7, 'Tạp chí Nội Thất 304 (Tháng 01.2021)', 8, 34500, 100, 'Ánh sáng có một vai trò rất quan trọng đối với mỗi ngôi nhà, nó là yếu tố chi phối cảm xúc, tâm hồn\r\nvà thậm chí cả sức khỏe người sống trong đó. Tất nhiên, một ngôi nhà đẹp rất cần và phải được thiết\r\nkế ánh sáng hợp lý. Vì vậy, tìm hiểu về ánh sáng sẽ giúp chúng ta biết cách tối ưu hóa ánh sáng trong\r\nngôi nhà của mình, trên nhiều phương diện: thẩm mỹ, sức khỏe, tiết kiệm năng lượng…\r\n\r\nNội Thất 304 phát hành ngày 1-1-2021 với chủ đề “ÁNH SÁNG” sẽ là một cẩm nang hữu ích, giới\r\nthiệu các xu hư', 'uploads/20210408044609c72d01adb054e1d89cb2b510151a12a8.jpg'),
(8, 'Máy Tính Khoa Học Casio FX-580VN X', 8, 561000, 50, 'Tính năng đột phá với 521 tính năng, vượt trội so với 453 tính năng dòng FX570VNP\r\nMàn hình LCD độ phân giải cao, hiển thị đầy đủ phép tính\r\nChất liệu bề mặt cao cấp, vân nổi 3D hiện đại, trẻ trung\r\nThiết kế riêng theo yêu cầu của học sinh và giáo viên Việt Nam\r\nPhù hợp với hình thức thi trắc nghiệm hiện nay\r\nGiao diện 2 ngôn ngữ: Anh - Việt\r\nTốc độ xử lý nhanh gấp 6 lần so với FX570VNPLUS\r\nLà sản phẩm duy nhất có tại Việt Nam và dành riêng cho thị trường Việt Nam\r\nĐược bộ GDĐT cho phép mang vào', 'uploads/2021040804503679165cb73b33931e9716152dab932640.jpg'),
(9, 'Bộ sách Làm Giàu Từ Chứng Khoán (How To Make Money In Stock) phiên bản mới + Hướng Dẫn Thực Hành CANSLIM Cho Người Mới Bắt Đầu', 9, 70000, 200, '“Làm Giàu Từ Chứng Khoán” là cuốn sách kinh điển mà mọi nhà đầu tư nên đọc, kể từ khi được phát hành lần đầu tiên vào năm 1988 sách đã bán được 2 triệu bản và được dịch ra nhiều thứ tiếng trên thế giới.', 'uploads/2021040804565564866db6603571411cc7e701ee05d993.jpg'),
(10, 'Combo 2 Cuốn: Tâm Lý Học Tội Phạm', 10, 210000, 100, '\"Tâm lý học tội phạm\" là bộ sách gồm 2 tập đề cập đến quyền lựa chọn, ý chí tự do, cái thiện và cái ác, phản ứng trước cám dỗ và sự thể hiện lòng dũng cảm hay hèn nhát khi đối mặt với nghịch cảnh của con người. Những cuốn sách thiêng liêng của các tôn giáo đều khuyên loài người không nên lừa dối, giận dữ và kiêu ngạo. Chúng ta nghĩ bản thân thế nào thì sẽ là như thế. Chúng ta không thể giúp một người từ bỏ tội ác và sống có trách nhiệm nếu không thể khiến anh ta thay đổi nhân tính, đó chính là \"', 'uploads/2021040804593731377e044579aa03901b4df1c9ae90fe.jpg'),
(11, 'Combo Giáo Trình Kỹ Thuật Lập Trình C Căn Bản Và Nâng Cao + Giáo Trình C++ Và Lập Trình Hướng Đối Tượng (2 quyển)', 10, 193000, 200, 'Giáo trình kỹ thuật lập trình C căn bản và nâng cao được hình thành qua nhiều năm giảng dạy của các tác giả. Ngôn ngữ lập trình C là một môn học cơ sở trong chương trình đào tạo kỹ sư, cử nhân tin học của nhiều trường đại học. Ở đây sinh viên được trang bị những kiến thức cơ bản nhất về lập trình, các kỹ thuật tổ chức dữ liệu và lập trình căn bản với ngôn ngữ C.', 'uploads/20210408050259d9d28ec0880f92f831ea946e4e08fdce.jpg');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `UserID` int(11) NOT NULL,
  `UserName` varchar(30) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `Password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`UserID`, `UserName`, `Email`, `Password`) VALUES
(1, 'Mỹ Nhi', 'mynhi2911@gmail.com', '25f9e794323b453885f5181f1b624d0b'),
(3, 'Nhi Hae ', 'nhihae2911@gmail.com', '25f9e794323b453885f5181f1b624d0b'),
(4, 'Mỹ Nhi', 'mynhi2911@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055'),
(5, 'mynhi', 'nhi@gmail.com', '202cb962ac59075b964b07152d234b70'),
(6, 'abc', 'abc@gmail.com', '202cb962ac59075b964b07152d234b70');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`CateID`);

--
-- Chỉ mục cho bảng `orderdetail`
--
ALTER TABLE `orderdetail`
  ADD KEY `ProductID` (`ProductID`),
  ADD KEY `OrderID` (`OrderID`);

--
-- Chỉ mục cho bảng `orderproduct`
--
ALTER TABLE `orderproduct`
  ADD PRIMARY KEY (`OrderID`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`ProductID`),
  ADD KEY `CateID` (`CateID`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UserID`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `CateID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `orderproduct`
--
ALTER TABLE `orderproduct`
  MODIFY `OrderID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `product`
--
ALTER TABLE `product`
  MODIFY `ProductID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `orderdetail`
--
ALTER TABLE `orderdetail`
  ADD CONSTRAINT `orderdetail_ibfk_1` FOREIGN KEY (`ProductID`) REFERENCES `product` (`ProductID`),
  ADD CONSTRAINT `orderdetail_ibfk_2` FOREIGN KEY (`OrderID`) REFERENCES `orderproduct` (`OrderID`);

--
-- Các ràng buộc cho bảng `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `product_ibfk_1` FOREIGN KEY (`CateID`) REFERENCES `category` (`CateID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

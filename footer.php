</div>
<div class="footer-dark">
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3 item">
                    <h3>Thành viên</h3>
                    <ul>
                        <li><a href="#">Đỗ Thị Mỹ Nhi</a></li>
                        <li><a href="#">Huỳnh Nhật Linh</a></li>
                        <li><a href="#">Phạm Thị Tố Uyên</a></li>
                    </ul>
                </div>
                <div class="col-sm-6 col-md-3 item">
                    <h3>Mã số sinh viên</h3>
                    <ul>
                        <li><a href="#">1711062217</a></li>
                        <li><a href="#">1711062124</a></li>
                        <li><a href="#">1711060382</a></li>
                    </ul>
                </div>
                <div class="col-md-6 item text">
                    <h3>Website bán sách</h3>
                    <p>Đồ án Phát triển phần mềm mã nguồn mở</p>
                </div>
                <div class="d-flex justify-content-center mt-4">
                    <span class="social"><i class="fa fa-google"></i></span>
                    <span class="social"><i class="fa fa-facebook"></i></span> 
                    <span class="social"><i class="fa fa-twitter"></i></span> 
                    <span class="social"><i class="fa fa-linkedin"></i></span> 
                </div>
            </div>
            <p class="copyright">Tên Gì Cũng Được © 2021</p>
        </div>
    </footer>
</div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
<script>
    src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"
</script>
</body>
</html>